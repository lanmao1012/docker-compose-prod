### 一、下载安装包

#### 下载地址

生产环境安装包：https://gitee.com/lanmao1012/docker-compose-prod

测试环境安装包：https://gitee.com/lanmao1012/docker-compose-test



#### 安装包说明

> **生产环境安装包内容**：
>
> ​	jar应用、tomcat应用、filebrowser组件、portainer组件
>
> **测试环境安装包内容**：
>
> ​	jar应用、tomcat应用、filebrowser组件、portainer组件、mysql、redis、mongo



#### 换行符替换（必做）

> 由于从Gitee上下载包中 shell脚本的换行符由LF转为了CRLF，所以上传到Linux服务器中需要做一次换行符转换，如用notepadd转换换行符，如下图

![](https://lanmao1012.gitee.io/knowledge/pages/deploy/container/apply/image/crlf.png)



#### 目录说明

```yaml
docker							// 根目录（上传到服务器的目录）			
    ├─bin						// 可执行文件目录
    │  ├─install.sh 			// 可执行文件-在线安装docker脚本
    │  ├─install-aliyun.sh		// 可执行文件-在线安装docker脚本（阿里云源）
    │  ├─install-offline.sh		// 可执行文件-离线安装docker脚本
    │  ├─shutdown.sh			// 可执行文件-应用停止脚本
    │  ├─startup.sh				// 可执行文件-应用启动脚本
    ├─filebrowser				// 文件上传组件目录
    │  ├─conf					// 文件上传组件-配置文件目录
    │  └─data					// 文件上传组件-数据目录（研发需要关注的文件目录）
    │      ├─backEnd			// 文件上传组件-后端存放目录
    │      │  ├─app				// 文件上传组件-后端应用
    │      │  └─conf			// 文件上传组件-后端应用配置文件
    │      ├─component			// 文件上传组件-组件相关目录
    │      │  └─nginx			// 文件上传组件-Nginx组件
    │      │      ├─conf		// 文件上传组件-Nginx组件配置文件
    │      │      └─logs		// 文件上传组件-Nginx组件日志文件
    │      └─frontEnd			// 文件上传组件-前端页面
    ├─mysql						// mysql组件目录
	│  └─init					// mysql组件目录-初始化脚本目录
    ├─redis						// redis组件目录
	│  └─conf					// redis组件目录-配置文件目录
    └─portainer					// 容器可视化组件目录
        └─data					// 容器可视化组件-数据目录
```



### 二、环境安装

环境要求：安装环境为CentOS7版本，尽可能是高版本，如果可以升级建议升级到最新版本 `yum -y update`

#### 在线安装

进入`docker-compose.yml`所在的目录，执行安装脚本进行docker环境的安装

```shell
# 1、赋权
chmod +x ./bin/*.sh
# 2、安装
sh ./bin/install.sh
```



#### 离线安装（脚本）

##### 下载安装包

> 链接：https://pan.baidu.com/s/1qbjmYvV4-Ov6Xj5ImEbd_w?pwd=6666 

注意：下载自己所需要的安装包，在`docker-compose.yml`文件所在的目录中创建文件夹名为`image`，上传下载的镜像到`image`文件夹（2个环境+4个镜像包），切记不要放错目录。

![](https://lanmao1012.gitee.io/knowledge/pages/deploy/container/apply/image/install-offline.png)

##### 执行安装脚本

> 默认脚本会安装`docker-23.0.1.tgz`和`docker-compose`
> 装镜像有：`potainer2.18.4.tar`、`nginx1220.tar`、`filebrowser2230.tar`、`xwq1012_tomcat8_jdk8.tar`

```shell
# 执行安装脚本（注意确保换行符已转换）
sh ./bin/install-offline.sh
```



#### 离线安装（手动）

##### 下载安装包

> 链接：https://pan.baidu.com/s/1qbjmYvV4-Ov6Xj5ImEbd_w?pwd=6666 

##### 安装docker

**1、安装**

```shell
# 复制docker-20.10.9.tgz到 /usr/bin下（usr/bin是环境变量目录，在路径下可以直接运行docker命令）
# 解压
tar xvf docker-20.10.9.tgz
# ls -l docker
cp docker/* /usr/bin
# rm -rf docker docker-20.10.9.tgz
```

**2、配置docker服务**

`vim /etc/systemd/system/docker.service`

```shell
# 
# 添加
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target

[Service]
Type=notify
ExecStart=/usr/bin/dockerd
ExecReload=/bin/kill -s HUP $MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target
```

**3、启动**

```shell
# 赋权限
sudo chmod +x /etc/systemd/system/docker.service

# 启动
sudo systemctl daemon-reload
sudo systemctl start docker
# 设置开机自启动
sudo systemctrl enable docker
```

**4、验证**

```shell
[root@bigdata003 docker]# docker version
Client: Docker Engine - Community
 Version:           19.03.2
 API version:       1.40
 Go version:        go1.12.8
 Git commit:        6a30dfc
 Built:             Thu Aug 29 05:28:55 2019
 OS/Arch:           linux/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          19.03.2
  API version:      1.40 (minimum version 1.12)
  Go version:       go1.12.8
  Git commit:       6a30dfc
  Built:            Thu Aug 29 05:27:34 2019
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.2.6
  GitCommit:        894b81a4b802e4eb2a91d1ce216b8817763c29fb
 runc:
  Version:          1.0.0-rc8
  GitCommit:        425e105d5a03fabd737a126ad93d62a9eeede87f
 docker-init:
  Version:          0.18.0
  GitCommit:        fec3683
```



##### 安装镜像

镜像下载后，按以下方式导入镜像

```shell
# 导入镜像
docker load -i potainer2.18.4.tar
docker load -i nginx1220.tar
docker load -i filebrowser2230.tar
docker load -i xwq1012_tomcat8_jdk8.tar
```



##### 安装docker-compose

**安装**

```shell
# 复制到 /usr/bin/下
cp -f ./docker-compose /usr/bin/docker-compose
# 赋执行权限
chmod +x /usr/bin/docker-compose
```



**测试**

```shell
[root@bigdata003 docker]# docker-compose version
docker-compose version 1.24.0, build 0aa59064
docker-py version: 3.7.2
CPython version: 3.6.8
OpenSSL version: OpenSSL 1.1.0j  20 Nov 2018
```



### 四、组件测试

启动应用：`sh ~/docker/bin/startup.sh`

停止应用：`sh ~/docker/bin/shutdown.sh`

启动应用后，测试应用及组件是否正常，以下系统都能正常访问和登录，即组件安装成功！

```shell
# 测试应用
jar应用：http://IP:8080/app-jar/test
tomcat应用：http://IP:8080/app-tomcat/test

# 测试文件上传组件
访问地址：http://IP:8080/file/
账号密码：super / 123456

# 测试容器组件
访问地址：http://IP:8080/portainer/
账号密码：super / super1234560
```



### 五、应用部署

#### 组件说明

##### filebrowser

> filebrowser 是一个使用go语言编写的软件，功能是可以通过浏览器对服务器上的文件进行管理。可以是修改文件，或者是添加删除文件，甚至可以分享文件，是一个很棒的文件管理器，你甚至可以当成一个网盘来使用。总之使用非常简单方便，功能很强大。

![](https://lanmao1012.gitee.io/knowledge/pages/deploy/container/apply/image/filerbrowser.png)

**说明：通过该组件进行项目文件的上传、更新操作，可以直接在线编辑文件，很方便。通过nginx代理到互联网后，直接在互联网在线更新项目。**



##### portainer

> Portainer是一款开源的容器管理平台，支持多种容器技术，如Docker、Kubernetes和Swarm等。它提供了一个易于使用的Web UI界面，可用于管理和监控容器和集群。Portainer旨在使容器管理更加简单和可视化，并且它适用于各种规模的容器环境，从个人计算机到企业级部署，而且能够一次性管理多种类型的多个集群。

![](https://lanmao1012.gitee.io/knowledge/pages/deploy/container/apply/image/portainer.png)

**说明：通过该组件可以进行项目的在线启停操作，可以很方便的在互联网就可以直接查看应用启动日志。**





#### 更新应用

> `docker-compose.yml`配置文件中的 app-jar和app-tomcat和对应`filebrowser`目录`data\backEnd`中的代码都是示例代码，真实使用后，请删除示例代码。

##### tomcat项目

1. 使用组件`filebrowser`将项目上传至`backEnd`目录
2. 修改配置文件`docker-compose.yml`修改服务节点`app-tomcat`或者复制新增服务节点（此步骤可选）
3. 重启服务并访问自己的项目，地址`http://IP:8002/你的项目名称` 如果要走`nginx`访问自己的应用，请修改对应的nginx配置文件，路径为`data\component\nginx\conf\default.conf`

##### jar项目

​	操作同`tomcat项目`部署方式

##### vue项目

1. 将前端代码复制到`filebrowser`组件中的`frontEnd`目录
2. 将后端代码复制到`filebrowser`组件中的`backEnd`目录
3. 其他操作同`tomcat项目`部署方式
