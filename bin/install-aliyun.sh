#!/bin/bash
# 更新yum依赖
#yum update -y && yum makecache -y
# 安装必要的一些系统工具
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# 添加软件源信息
sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo sed -i 's+download.docker.com+mirrors.aliyun.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo

# 更新并安装Docker-CE
sudo yum makecache fast
sudo yum -y install docker-ce


# 设置开机启动服务
sudo systemctl enable docker

# 启动服务
sudo systemctl start docker

# 配置docker镜像加速
[ -f /etc/docker/daemon.json ] || touch /etc/docker/daemon.json
cat >>/etc/docker/daemon.json <<EOF
{
"registry-mirrors": ["https://gdhauhuq.mirror.aliyuncs.com"],
"log-driver":"json-file",
"log-opts":{"max-size" :"500m","max-file":"3"}
}
EOF

# 重新加载docker配置
sudo systemctl daemon-reload
sudo systemctl restart docker

# 安装epel源
sudo yum install -y epel-release

# 安装docker-compose，如果没有python3会安装python3
sudo yum install -y docker-compose

# 查看版本信息
sudo docker -v
sudo docker-compose -v
echo "Install completed!"