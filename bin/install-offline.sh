# 解压
tar xvf ./image/docker-23.0.1.tgz -C ./image
# ls -l docker
cp ./image/docker/* /usr/bin
# rm -rf docker docker-20.10.9.tgz

# 配置docker服务
cat >>/etc/systemd/system/docker.service <<EOF
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target

[Service]
Type=notify
ExecStart=/usr/bin/dockerd
ExecReload=/bin/kill -s HUP $MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target
EOF

# 赋权限
sudo chmod +x /etc/systemd/system/docker.service

# 设置开机启动服务
sudo systemctl enable docker

# 启动服务
sudo systemctl daemon-reload
sudo systemctl start docker

# 复制到 /usr/bin/下
cp -f ./image/docker-compose /usr/bin/docker-compose
# 赋执行权限
chmod +x /usr/bin/docker-compose

# 导入镜像
docker load -i ./image/potainer2.18.4.tar
docker load -i ./image/nginx1220.tar
docker load -i ./image/filebrowser2230.tar
docker load -i ./image/xwq1012_tomcat8_jdk8.tar

# 查看版本信息
sudo docker -v
sudo docker-compose -v
echo "Install completed!"

