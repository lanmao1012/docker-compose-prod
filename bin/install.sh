#!/bin/bash
# 更新yum依赖
#yum update -y && yum makecache -y
# 安装yum-utils
sudo yum install -y yum-utils

# 配置yum源
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 安装docker-ce
sudo yum install -y docker-ce

# 设置开机启动服务
sudo systemctl enable docker

# 启动服务
sudo systemctl start docker

# 配置docker镜像加速
[ -f /etc/docker/daemon.json ] || touch /etc/docker/daemon.json
cat >>/etc/docker/daemon.json <<EOF
{
"registry-mirrors": ["https://gdhauhuq.mirror.aliyuncs.com"],
"log-driver":"json-file",
"log-opts":{"max-size" :"500m","max-file":"3"}
}
EOF

# 重新加载docker配置
sudo systemctl daemon-reload
sudo systemctl restart docker

# 安装epel源
sudo yum install -y epel-release

# 安装docker-compose，如果没有python3会安装python3
sudo yum install -y docker-compose

# 查看版本信息
sudo docker -v
sudo docker-compose -v
echo "Install completed!"

